import $ from 'jquery'; 
import waypoints from '../../../../node_modules/waypoints/lib/noframework.waypoints'; 


class Shadows {
    constructor() {  
        this.shadow = $('#lion-shadow');  
        this.count = 12; 
        this.scroll(); 
    }
    
    scrollDemo() { 
        var item = this.itemToReveal; 
         $(window).scroll(function() {
           var hT = $('.coverPage--Quote').offset().top,
               hH = $('.coverPage--Quote').outerHeight(),
               wH = $(window).height(),
               wS = $(this).scrollTop();
            console.log((hT-wH) , wS);
           if (wS > (hT+hH-wH)){
            item.addClass('backgroundPics--lion-largeShadow');
           } else if (wS < (hT+hH-wH)) {
               item.removeClass('backgroundPics--lion-largeShadow'); 
            }
        });
    }   
     
    scroll() {  
        var count = this.count;  
        var shadow = this.shadow; 
        $(window).scroll(function() {
        count = $(this).scrollTop(); 
        
            var countHeight = count * .50 + 500;
            var countPadding = count * .75;       
            var countMargin = count * -.05 + -120;           
            console.log(count);    
            
            if (countHeight < 900) {
            shadow.css({ 'padding-top' : countPadding, 'height' : countHeight, 'margin-left' : countMargin, 'padding-left': countMargin}); 
            }
						
        });    
    }  
}                                         
    
export default Shadows;                         

